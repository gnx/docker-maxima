FROM ubuntu:latest

MAINTAINER Hristo Hristov hristo.dr.hristov@gmail.com

ENV DEBIAN_FRONTEND=nonintercative 
RUN apt-get update && apt-get upgrade && apt-get install -y maxima maxima-share && apt-get clean
